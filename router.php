<?php

require_once 'lib/tabla.php';

define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');

$accion = $_GET['action'];

if ($accion == '') {
    echo 'ERROR! Ruta inválida';
} else {
    $parametros = explode('/', $accion);
   
    switch ($parametros[0]) {
        case 'inicio':
            showMenu();
            break;
            
        case 'tabla':
            $limite = $parametros[1];
            showTable($limite);
            break;
        
        case 'desarrolladores':
            showDevelopers();
            break;    

        default:
            showError();
            break;
    }
}

?>