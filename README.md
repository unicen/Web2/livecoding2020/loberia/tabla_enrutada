# tabla_enrutada

Modifique el ejercicio de la tabla de multiplicar para enrutar la misma. Se debe generar un menú que tenga accesos directos a los límites de la tabla más usados. 

Ejemplo:
     /tabla/5
     /tabla/10
     /tabla/20