<?php

function showTable($limite) {
    echo '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <base href='.BASE_URL.'>
            <title>Tabla de Multiplicar '. $limite . '</title>
            <link rel="stylesheet" href="css/style.css">
        </head>
        <body>
    ';

    echo "<h2><a href='inicio'>Inicio</a></h2>";

    // imprime la tabla de multiplicar
    echo "<table>";
    for ($fila=0; $fila<=$limite; $fila++) {
        echo "<tr>";
        for ($col=0; $col<=$limite; $col++) {
            if ($col == 0)
                echo "<td class='resaltado'>" . $fila . "</td>";
            elseif ($fila == 0)
                echo "<td class='resaltado'>" . $col . "</td>";
            elseif ($fila == $col)
                echo "<td class='resaltado'>" . ($fila*$col) . "</td>";
            else
                echo "<td>" . ($fila*$col) . "</td>";
        }
        echo "</tr>";
    }
    echo "</table>";

    echo '
        </body>
        </html>
    ';
}

function showError() {
    echo '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <base href='.BASE_URL.'>
            <title>Error</title>
            <link rel="stylesheet" href="css/style.css">
        </head>
        <body>
    ';

    echo '<h1>Error 404</h1>';
    echo '<h2>Recurso no encontrado!</h2>';

    echo '
        </body>
        </html>
    ';
}

function showDevelopers() {
    echo '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <base href='.BASE_URL.'>
            <title>Desarroladores</title>
            <link rel="stylesheet" href="css/style.css">
        </head>
        <body>
    ';

    echo "<h2><a href='inicio'>Inicio</a></h2>";

    echo '<h1>Desarrolladores - Loberia</h1>';

    echo '<ul>';    
    echo '<li>Julio</li>';
    echo '<li>Soledad</li>';
    echo '<li>Francisco</li>';
    echo '</ul>';

    echo '<img src=imgs/developers.jpg>';

    echo '
        </body>
        </html>
    ';
}

function showMenu() {
    echo '
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <base href='.BASE_URL.'>
            <title>Inicio</title>
            <link rel="stylesheet" href="css/style.css">
        </head>
        <body>
    ';

    echo '<h1>Accesos directos - Tablas frecuentes</h1>';

    echo '<ul>';
    for ($i=5; $i<=20; $i+=2) {
        echo "<li><a href='tabla/{$i}'>Tabla de Multiplicar del {$i} </a></li>";
    } 
    echo '</ul>';

    echo "<h2><a href='desarrolladores'>Información sobre los desarrolladores</a></h2>";

    echo '
        </body>
        </html>
    ';
}

?>